const EventEmitter = require('events');
const logger = require('./util/log');
const history = require('./util/history');

/*
 *      id=<uuidV4>
 *      game_mode=3
 *      inet=true
 *      numpl=1
 *      maxpl=4
 *      nation=de
 *      password=false
 *      players=[noBlubb]
 *      server_addr=xxx.xxx.xxx.xxx
 *      server_name=Bushton Mod V3
 *      server_port=58282
 *      vmaj=1
 *      vmin=0
 *      vtype=f
 *      mod=Bushton Mod V3
 *      updated_at=<Date>
 *      created_at=<Date>
 *      ended_at=<Date>
 */

class Repository extends EventEmitter {
    constructor(sessions = []) {
        super();
        this.sessions = sessions;
        this.protected = ['id', 'created_at', 'updated_at'];
        this.timeout = setInterval(() => {
            const now = Date.now();
            const t = this;
            this.sessions = this.sessions.filter(session => {
                const expired = now - session.updated_at > 180000;
                const superExpired = now - session.updated_at > 60000 * 20;
                if (expired && (session.run || !session.server_name) || superExpired) {
                    logger.info(`Session ${session.id} has expired`, { session });
                    session.ended_at = now;
                    t.emit('ended', session);
                    return false;
                }
                return true;
            });
        }, 15000);
    }

    getSessions() {
        return this.sessions;
    }

    create(values, id) {
        logger.debug(`Creating new session ${id}`, { id });
        const session = Object.assign(values, { id, created_at: Date.now(), updated_at: Date.now() });
        this.sessions.push(session);

        this.emit('update', this.sessions);
    }

    update(values, id) {
        logger.debug('Received update for session', { values, id });
        let session = this.sessions.find(session => session.id === id);
        if (session === undefined) {
            logger.warn(`Unable to update session ${id}`, { id });
            return false;
        }

        for (let field of Object.keys(values)) {
            if (this.protected.includes(field)) {
                continue;
            }
            session[field] = values[field];
        }
        session.updated_at = Date.now();

        if (values.run) {
            history.sessionStarted(session);
        }

        this.emit('update', this.sessions);
        return true;
    }

    alive(address, alive) {
        const session = this.sessions.find(session => session.server_addr === address);
        logger.debug('Received alive message %s', { address, alive })
        if (session) {
            if (alive) {
                logger.debug('Updating session %s', { id: session.id });
                this.update({}, session.id);
                return;
            } else {
                logger.debug('Stopping session %s', { id: session.id });
                this.sessions = this.sessions.filter(s => s.id != session.id);
                session.ended_at = Date.now();
                this.emit('ended', session);
                return;
            }
        }
        logger.error('Unable to find session for keep alive', { address, sessions: this.sessions });
    }

    connectivity(address) {
        const session = this.sessions.find(session => session.server_addr === address);
        logger.debug(`Connectivity ok for ${address}`, { address })
        if (session) {
            this.update({ connectivity: true }, session.id);
            return;
        }
        logger.error('Unable to find session for connectivity response', { address, sessions: this.sessions });
    }
};

module.exports = Repository;
