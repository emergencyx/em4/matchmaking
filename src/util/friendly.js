const badStrings = [
    'c2V4',
    'Zmljaw==',
    'ZnVjaw==',
    'a3JpZWc=',
    'bmF6aQ==',
    'aGl0bGVy',
    'cGVuaXM=',
    'YXVzY2h3aXR6'
];

const compiled = badStrings.map(badString => {
    const decoded = Buffer.from(badString, 'base64').toString();

    return {
        detector: new RegExp(decoded, 'ig'),
        replace: '♡'.repeat(decoded.length)
    }
});

function filter(input) {
    for (let test of compiled) {
        if (test.detector.test(input)) {
            return input.replace(test.detector, test.replace);
        }
    }
    return input;
}

module.exports = {
    filter
};
