const request = require('superagent');
const filterHistory = require('./serializer').filterHistory;

const historyConfig = { api: '', authorization: '' };

function reportSessionEnded(session, onRequestComplete) {
    request.post(historyConfig.api).
        set('Authorization', historyConfig.authorization).
        set('Accept', 'application/json').
        send(filterHistory(session)).
        end(onRequestComplete);
}

module.exports = {
    historyConfig,
    reportSessionEnded,
};
