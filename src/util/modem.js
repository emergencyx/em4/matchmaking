function applyModem(session) {
    if (session.modem) {
        return Object.assign({}, session, {
            server_addr: session.modem.ip,
            server_port: session.modem.port
        });
    }
    return session;
}

module.exports = { applyModem };
