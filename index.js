require('dotenv').config();

const logger = require('./src/util/log');
const Repository = require('./src/repository');
const UdpEndpoint = require('./src/endpoint/udp');
const TcpEndpoint = require('./src/endpoint/tcp');
const ConnectivityEndpoint = require('./src/endpoint/connectivity');

if (process.env.SENTRY_DSN) {
    const Sentry = require('@sentry/node');
    Sentry.init({ dsn: process.env.SENTRY_DSN });
}

if (!process.env.APP_TOKEN) {
    throw new Error('Missing APP_TOKEN - abort start');
}

const repository = new Repository();
const app = require('./src/endpoint/http')(repository);
app.listen(process.env.APP_PORT, () => {
    logger.info(`http::up ${process.env.APP_PORT}`);
});

if (process.env.HISTORY_API_ENDPOINT) {
    const history = require('./src/util/history-client');
    history.historyConfig.api = process.env.HISTORY_API_ENDPOINT;
    history.historyConfig.authorization = process.env.HISTORY_API_TOKEN;
    repository.on('ended', (session) => {
        history.reportSessionEnded(session, (err, res) => {
            if (err) {
                logger.error("failed to send session to the history API", { err });
            }
        });
    });
}

const udp = new UdpEndpoint(repository);
udp.udpHost.bind(process.env.EM4_MASTER_SERVER_PORT || 54321);

const connectivity = new ConnectivityEndpoint(repository);
connectivity.udpHost.bind(12345);

const tcp = new TcpEndpoint(repository);
tcp.tcpHost.listen({ host: '0.0.0.0', port: process.env.EM4_MASTER_SERVER_PORT || 54321 });
