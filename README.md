Emergency 4 Matchmaking Server
==============================
The matchmaking server exchanges information (i.e. session name, server port) between hosts and other players.    
Hosts announce their session to the matchmaking server and players can query the server for a list of running sessions.    

This project replaces the official matchmaking server with an compatible implementation.    
Additionally, an HTTP API is exposed that allows for easier consumption in other applications.
- [EmergencyX Website](https://www.emergencyx.de/multiplayer/) 
- [Emergency Fanforum](http://www.emergency-forum.de/index.php?thread/63678-/)

# 1. Installation
- `yarn install`
- `yarn test`
- `cp .env.example .env`
- edit `.env`

Add an entry to your hosts file for `matchmaking.emergencyx.de` to resolve to your own server or deploy your own [versix API](https://gitlab.com/emergencyx/em4/versix).

# 2. Communication
Emergency 4 communicates with the Matchmaking Server by using UDP and TCP protocols.    
You can find examples in the `test/` directory.

### UDP (Port 54321)
UDP messages are exchanged to query running sessions or as keep-alive messages by the host.    
Messages are sent as UDP payload using the structure `{transactionID}{messageType}{messageLength}{message}`:

| Field         | Type   | Size                   | Use                                             |
| -----         | ----   | ----                   | ---                                             |
| transactionID | bytes  | 2                      | Response messages use the same transactionID    |
| messageType   | byte   | 1                      | See message types listed below                  |
| messageLength | byte   | 1                      | Unsigned integer length of the message in bytes |
| message       | string | value of messageLength | See message types listed below                  |

#### Query Sessions (messageType `0x00`)
The client can query running sessions by sending a message of type `0x00`.    
The parameters `game` and `lang` are required. `mod` may be empty.

- `game=EM4;lang=de;mod=`
- `game=EM4;lang=de;mod=BFEMP`

#### Keep Alive (messageType `0x00`)
Signal used by the host that the current session is still running.    
This message is sent once every two minutes. The parameters `game` and `lang` are required.

- `game=EM4;alive=0` to terminate the session
- `game=EM4;alive=1`

### TCP (Port 54321)
The host establishes a TCP connection to the Matchmaking Server to update the session while all players are in the lobby.
Once the game starts, the connection is closed and keep alive messages are sent to signal the session state.
