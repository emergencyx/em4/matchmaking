const dgram = require('dgram');
const expect = require('chai').expect;
const sinon = require('sinon');
const Repository = require('../src/repository');
const UdpEndpoint = require('../src/endpoint/udp');

describe('UDP API', function () {
    let repository;
    let clock;
    beforeEach((done) => {
        this.timeout(1000);
        clock = sinon.useFakeTimers({ now: Date.now() });
        repository = new Repository();
        udpEndpoint = new UdpEndpoint(repository);
        udpEndpoint.udpHost.bind({ address: '127.0.0.1' }, done);
    });

    it('should return sessions and proper transaction id', function (done) {
        this.timeout(1000);
        const request = Buffer.from('6e8b001567616d653d454d343b6c616e673d75733b6d6f643d', 'hex');
        const response = Buffer.from('6e8b80ae67616d655f6d6f64653d333b696e65743d313b6d6178706c3d343b6e6174696f6e3d64653b6e756d706c3d313b70617373776f72643d303b706c61796572733da7283130291b6e6f426c7562621ca7282d31291b3b7365727665725f616464723d38342e3230302e3130362e3135343b7365727665725f6e616d653d456d657267656e6379583b7365727665725f706f72743d35383238323b766d616a3d313b766d696e3d303b76747970653d66', 'hex');

        repository.create({
            game_mode: 3,
            inet: true,
            maxpl: 4,
            nation: 'de',
            numpl: 1,
            password: false,
            players: [
                'noBlubb'
            ],
            server_addr: '84.200.106.154',
            server_name: 'EmergencyX',
            server_port: 58282,
            vmaj: 1,
            vmin: 0,
            vtype: 'f'
        }, 'test-id');

        const { address, port } = udpEndpoint.udpHost.address();

        const client = dgram.createSocket('udp4');
        client.once('message', (data, rinfo) => {
            client.close();
            expect(data).to.be.deep.equal(response);
            done();
        });

        client.connect(port, address, (err) => {
            if (err) {
                client.close();
                this.fail(err);
            }
            client.send(request, (err) => {
                if (err) {
                    client.close();
                    this.fail(err);
                }
            });
        });
    });

    it('should return sessions filtered by modification', function (done) {
        this.timeout(1000);
        const request = Buffer.from('5e33001a67616d653d454d343b6c616e673d64653b6d6f643d4246454d50', 'hex');
        const response = Buffer.from('5e3380c972756e3d313b7365727665725f616464723d3138382e36332e36312e35363b696e65743d313b67616d655f6d6f64653d333b6d6178706c3d323b6d6f643d4246454d503b6e6174696f6e3d75733b6e756d706c3d323b70617373776f72643d303b706c61796572733da7283130291b44656c74611ca7282d31291b2c2043797275733b7365727665725f6e616d653d42494542455246454c44202844656c7461293b7365727665725f706f72743d35383238323b766d616a3d313b766d696e3d333b76747970653d66', 'hex');

        // add a session with a mod to the selection 
        // run=1;server_addr=188.63.61.56;inet=1;game_mode=3;maxpl=2;mod=BFEMP;nation=us;numpl=2;password=0;
        // players=(10)Delta(-1), Cyrus;server_name=BIEBERFELD (Delta);server_port=58282;vmaj=1;vmin=3;vtype=f
        repository.create({
            game_mode: 3,
            inet: true,
            maxpl: 4,
            nation: 'de',
            numpl: 1,
            password: false,
            players: [
                'noBlubb'
            ],
            server_addr: '84.200.106.154',
            server_name: 'EmergencyX',
            server_port: 58282,
            vmaj: 1,
            vmin: 0,
            vtype: 'f'
        }, 'test-id');
        repository.create({
            run: true,
            server_addr: '188.63.61.56',
            inet: true,
            game_mode: 3,
            maxpl: 2,
            mod: 'BFEMP',
            nation: 'us',
            numpl: 2,
            password: false,
            players: [
                'Delta',
                'Cyrus'
            ],
            server_name: 'BIEBERFELD (Delta)',
            server_port: 58282,
            vmaj: 1,
            vmin: 3,
            vtype: 'f'
        }, 'test-id-with-mod');

        const { address, port } = udpEndpoint.udpHost.address();

        const client = dgram.createSocket('udp4');
        client.once('message', (data, rinfo) => {
            client.close();
            expect(data).to.be.deep.equal(response);
            done();
        });

        client.connect(port, address, (err) => {
            if (err) {
                client.close();
                this.fail(err);
            }
            client.send(request, (err) => {
                if (err) {
                    client.close();
                    this.fail(err);
                }
            });
        });
    });

    it('should handle keep alive messages', function () {
        this.timeout(1000);
        const keepAliveTrue = Buffer.from('616c6976653d313b67616d653d454d34', 'hex');
        const keepAliveFalse = Buffer.from('616c6976653d303b67616d653d454d34', 'hex');

        repository.create({
            run: true,
            server_addr: '127.0.0.1',
            inet: true,
            game_mode: 3,
            maxpl: 2,
            mod: 'BFEMP',
            nation: 'us',
            numpl: 2,
            password: false,
            players: [
                'Delta',
                'Cyrus'
            ],
            server_name: 'BIEBERFELD (Delta)',
            server_port: 58282,
            vmaj: 1,
            vmin: 3,
            vtype: 'f'
        }, 'test-id-with-mod');

        expect(repository.getSessions()).to.be.lengthOf(1);
        expect(repository.getSessions()[0]).to.include({ server_name: 'BIEBERFELD (Delta)' });

        clock.tick(45000);
        udpEndpoint.handleMasterRequest(1, keepAliveTrue, { address: '127.0.0.1' });
        expect(repository.getSessions()).to.be.lengthOf(1);
        expect(repository.getSessions()[0]).to.include({ server_name: 'BIEBERFELD (Delta)' });

        clock.tick(45000);
        udpEndpoint.handleMasterRequest(2, keepAliveTrue, { address: '127.0.0.1' });
        expect(repository.getSessions()).to.be.lengthOf(1);
        expect(repository.getSessions()[0]).to.include({ server_name: 'BIEBERFELD (Delta)' });

        clock.tick(45000);
        udpEndpoint.handleMasterRequest(3, keepAliveTrue, { address: '127.0.0.1' });
        expect(repository.getSessions()).to.be.lengthOf(1);
        expect(repository.getSessions()[0]).to.include({ server_name: 'BIEBERFELD (Delta)' });

        clock.tick(45000);
        udpEndpoint.handleMasterRequest(4, keepAliveFalse, { address: '127.0.0.1' });
        expect(repository.getSessions()).to.be.empty;
    });

    it('should not fail on malformed messages (regression test #10)', function (done) {
        this.timeout(1000);
        const request = Buffer.from('1234', 'hex');
        const { address, port } = udpEndpoint.udpHost.address();

        const client = dgram.createSocket('udp4');
        client.connect(port, address, (err) => {
            if (err) {
                client.close();
                fail(err);
            }
            client.send(request, (err) => {
                client.close();
                if (err) {
                    fail(err);
                }
                done();
            });
        });
    });

    afterEach(function (done) {
        clearInterval(repository.timeout);
        clock.uninstall();
        udpEndpoint.udpHost.close(done);
    });
});
