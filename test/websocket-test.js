const Repository = require('../src/repository');

const chai = require('chai');
chai.use(require('chai-http'));
const expect = chai.expect;
const WebSocket = require('ws');

describe('WebSocket API', function () {
    let app;
    let repository;

    before(function () {
        repository = new Repository([{
            server_name: 'test',
            nation: 'de',
            mod: '',
            players: ['noBlubb', 'ciajoe'],
            password: false,
            run: false,
            maxpl: 4,
            numpl: 2,
            id: 'test-id'
        }]);
        app = require('../src/endpoint/http')(repository);
        clearInterval(repository.timeout);
    });

    it('should receive initial state on connect', function (done) {
        app.listen(0, '127.0.0.1', () => {
            const ws = new WebSocket(`ws://${app.address().address}:${app.address().port}/websocket`);
            ws.onmessage = (message) => {
                const body = JSON.parse(message.data);
                expect(body).to.be.deep.equal([{
                    id: 'test-id',
                    name: 'test', players: ['noBlubb', 'ciajoe'],
                    nation: 'de',
                    mod: '',
                    password: false,
                    started: false,
                    maxpl: 4,
                    numpl: 2,
                    connectivity: false,
                    modem: false
                }]);
                ws.close();
                app.close(done);
            };
        });
    });
});
