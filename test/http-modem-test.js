const assert = require('assert');
const chai = require('chai');
chai.use(require('chai-http'));
const expect = chai.expect;

const TOKEN = 'token';
process.env.APP_TOKEN = TOKEN;

const Repository = require('../src/repository');

describe('HTTP MODEM API', function () {
    let app;
    let repository;
    let requester;

    before(function () {
        repository = new Repository([{
            id: 'modem-test',
            server_name: 'test',
            nation: 'de',
            mod: '',
            players: ['noBlubb', 'ciajoe'],
            password: false,
            run: false,
            maxpl: 4,
            numpl: 2
        }]);
        app = require('../src/endpoint/http')(repository);
        requester = chai.request(app).keepOpen();
        assert.ok(requester, 'Test server not initialized');
    });

    it('requires authentication (http 401)', function (done) {
        requester
            .post('/api/modem')
            .end(function (err, res) {
                expect(res).to.have.status(401);
                done();
            });
    });

    it('requires a valid configuration body (http 400)', function (done) {
        requester
            .post('/api/modem')
            .auth('modem', TOKEN)
            .end(function (err, res) {
                expect(res).to.have.status(400);
                done();
            });
    });

    it('returns http 410 (gone) if the session does not exist', function (done) {
        requester
            .post('/api/modem')
            .type('json')
            .auth('modem', TOKEN)
            .send({ id: 'bad-test', modem: { ip: '127.0.0.1', port: 50000 } })
            .end(function (err, res) {
                expect(res).to.have.status(410);
                done();
            });
    });

    it('requires a body payload', function (done) {
        requester
            .post('/api/modem')
            .type('json')
            .auth('modem', TOKEN)
            .send({ id: 'modem-test', modem: { ip: '127.0.0.1', port: 50000 } })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                expect(repository.getSessions()[0]).to.deep.include({ id: 'modem-test', modem: { ip: '127.0.0.1', port: 50000 } });
                done();
            });
    });

    after(function () {
        requester.close();
        clearInterval(repository.timeout);
    });
});
