const expect = require('chai').expect;
const parser = require('../src/util/parser');
const assert = require('assert');

describe('Message Parser Unit Tests', () => {
    describe('Messages Over UDP', () => {
        it('should parse keep alive messages (not alive)', () => {
            const request = Buffer.from('game=EM4;alive=0');
            const parsed = parser.request(request);
            expect(parsed).to.be.deep.equal({ game: 'EM4', alive: false });
        });

        it('should parse keep alive messages (alive)', () => {
            const request = Buffer.from('game=EM4;alive=1');
            const parsed = parser.request(request);
            expect(parsed).to.be.deep.equal({ game: 'EM4', alive: true });
        });

        it('should parse queries', () => {
            const request = Buffer.from('game=EM4;lang=de;mod=');
            const parsed = parser.request(request);
            expect(parsed).to.be.deep.equal({ game: 'EM4', lang: 'de', mod: '' });
        });

        it('should parse queries with mod name', () => {
            const request = Buffer.from('game=EM4;lang=de;mod=BFEMP');
            const parsed = parser.request(request);
            expect(parsed).to.be.deep.equal({ game: 'EM4', lang: 'de', mod: 'BFEMP' });
        });

        it('should parse queries with more complicated mod name', () => {
            const request = Buffer.from('game=EM4;lang=de;mod=Harbor City 4.6.1 Public');
            const parsed = parser.request(request);
            expect(parsed).to.be.deep.equal({ game: 'EM4', lang: 'de', mod: 'Harbor City 4.6.1 Public' });
        });
    });

    describe('Messages Over TCP', () => {
        it('should parse create session messages', () => {
            const request = Buffer.of(0x67, 0x61, 0x6d, 0x65,
                0x5f, 0x6d, 0x6f, 0x64, 0x65, 0x3d, 0x33, 0x3b,
                0x6d, 0x61, 0x78, 0x70, 0x6c, 0x3d, 0x34, 0x3b,
                0x6d, 0x6f, 0x64, 0x3d, 0x47, 0x61, 0x6e, 0x64,
                0x69, 0x61, 0x20, 0x6d, 0x6f, 0x64, 0x20, 0x42,
                0x45, 0x54, 0x41, 0x3b, 0x6e, 0x61, 0x74, 0x69,
                0x6f, 0x6e, 0x3d, 0x65, 0x73, 0x3b, 0x6e, 0x75,
                0x6d, 0x70, 0x6c, 0x3d, 0x31, 0x3b, 0x70, 0x61,
                0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x3d, 0x30,
                0x3b, 0x70, 0x6c, 0x61, 0x79, 0x65, 0x72, 0x73,
                0x3d, 0xa7, 0x28, 0x31, 0x30, 0x29, 0x1b, 0x4d,
                0x61, 0x72, 0x63, 0x6f, 0x1c, 0xa7, 0x28, 0x2d,
                0x31, 0x29, 0x1b, 0x3b, 0x73, 0x65, 0x72, 0x76,
                0x65, 0x72, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x3d,
                0x4d, 0x61, 0x72, 0x63, 0x6f, 0x20, 0x28, 0x4d,
                0x61, 0x72, 0x63, 0x6f, 0x29, 0x3b, 0x73, 0x65,
                0x72, 0x76, 0x65, 0x72, 0x5f, 0x70, 0x6f, 0x72,
                0x74, 0x3d, 0x35, 0x38, 0x32, 0x38, 0x32, 0x3b,
                0x76, 0x6d, 0x61, 0x6a, 0x3d, 0x31, 0x3b, 0x76,
                0x6d, 0x69, 0x6e, 0x3d, 0x33, 0x3b, 0x76, 0x74,
                0x79, 0x70, 0x65, 0x3d, 0x66);
            const parsed = parser.request(request);
            assert.deepStrictEqual(parsed, {
                "game_mode": 3,
                "maxpl": 4,
                "mod": "Gandia mod BETA",
                "nation": "es",
                "numpl": 1,
                "password": false,
                "players": [
                    "Marco"
                ],
                "server_name": "Marco (Marco)",
                "server_port": 58282,
                "vmaj": 1,
                "vmin": 3,
                "vtype": "f",
            });
        });
    });

    describe('Player Names Field', () => {
        it('should decode a single player name', () => {
            const value = Buffer.of(0xa7, 0x28, 0x31, 0x30, 0x29, 0x1b, 0x4d,
                0x61, 0x72, 0x63, 0x6f, 0x1c, 0xa7, 0x28, 0x2d, 0x31, 0x29, 0x1b);
            const parsed = parser.decodePlayers(value);
            expect(parsed).to.be.deep.equal(['Marco']);
        });

        it('should decode multiple player names', () => {
            const value = Buffer.of(0xa7, 0x28, 0x31, 0x30,
                0x29, 0x1b, 0x52, 0x65, 0x78, 0x66, 0x69, 0x72,
                0x65, 0x1c, 0xa7, 0x28, 0x2d, 0x31, 0x29, 0x1b,
                0x2c, 0x20, 0x73, 0x65, 0x62, 0x61, 0x33, 0x66,
                0x69, 0x72, 0x65, 0x2c, 0x20, 0x39, 0x31, 0x31,
                0x20, 0x47, 0x69, 0x6f, 0x76, 0x69, 0x6e, 0x6e,
                0x79);
            const parsed = parser.decodePlayers(value);
            expect(parsed).to.be.deep.equal(['Rexfire', 'seba3fire', '911 Giovinny']);
        });
    });
});
