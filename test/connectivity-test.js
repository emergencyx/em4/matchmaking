const expect = require('chai').expect;
const Repository = require('../src/repository');
const ConnectivityEndpoint = require('../src/endpoint/connectivity');

describe('Connectivity Checker', function () {
    let repository;
    let connectivityChecker;
    beforeEach(() => {
        this.timeout(1000);
        repository = new Repository();
        connectivityChecker = new ConnectivityEndpoint(repository);
    });

    it('should verify that the "connectivity check" correctly identifies a host that uses a proper port setup', () => {
        this.timeout(1000);

        const rinfo = { "address": "84.200.106.154", "family": "IPv4", "port": 12345, "size": 166 };
        const message = Buffer.from('35ee80a267616d655f6d6f64653d333b6d6178706c3d323b6d6f643d4246454d50323032303b6e6174696f6e3d75733b6e756d706c3d323b70617373776f72643d313b706c61796572733da7283130291b46616269616e1ca7282d31291b2c20456464793b7365727665725f6e616d653d46463830202846616269616e293b7365727665725f706f72743d35383238323b766d616a3d313b766d696e3d333b76747970653d66', 'hex');

        repository.create({
            game_mode: 3,
            inet: true,
            maxpl: 4,
            nation: 'de',
            numpl: 1,
            password: false,
            players: [
                'noBlubb'
            ],
            server_addr: '84.200.106.154',
            server_name: 'EmergencyX',
            server_port: 58282,
            vmaj: 1,
            vmin: 0,
            vtype: 'f'
        }, 'connectivity-test-id');

        connectivityChecker.onGameResponse(message, rinfo);

        const sessions = repository.getSessions();
        expect(sessions).to.have.lengthOf(1);
        expect(sessions[0]).to.deep.include({
            server_name: 'EmergencyX',
            connectivity: true
        });
    });

    afterEach(() => {
        clearInterval(repository.timeout);
    });
});
