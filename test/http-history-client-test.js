const expect = require('chai').expect;
const request = require('superagent');
const requestMock = require('superagent-mock');
const config = require('./http-history-client-mocks.js');
const { reportSessionEnded, historyConfig } = require('../src/util/history-client.js');

describe('HTTP History API Client', function () {
    let requester;

    before(function () {
        requester = requestMock(request, config);
        historyConfig.api = 'https://history-service.emergencyx.de/api/v1/sessions';
    });

    after(function () {
        requester.unset();
    });

    it('should post a session to the history API', function (done) {
        const session = {
            id: 'friendly-test',
            name: '♡♡♡♡♡',
            players: ['a', 'b'],
            nation: 'de',
            mod: '',
            password: false,
            started: false,
            maxpl: 4,
            numpl: 2,
            connectivity: true,
            modem: false,
            created_at: 1700150000000,
            ended_at: 1700160000000,
        };
        historyConfig.authorization = 'valid-token';

        reportSessionEnded(session, (err, res) => {
            expect(err).to.not.exist;
            expect(res.data).to.deep.equal({
                name: 'Unbekannter Server',
                numpl: 2,
                maxpl: 4,
                mod: '',
                nation: 'de',
                started: false,
                password: false,
                connectivity: true,
                id: 'friendly-test',
                modem: false,
                created_at: '2023-11-16T15:53:20Z',
                ended_at: '2023-11-16T18:40:00Z',
            });

            done();
        });
    });

    it('should report error to the callback handler', function (done) {
        const session = {
            id: 'friendly-test',
        };
        historyConfig.authorization = 'totally-not-a-valid-token';

        reportSessionEnded(session, (err, res) => {
            expect(err).to.exist;
            done();
        });
    });
});
