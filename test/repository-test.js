const Repository = require('../src/repository');
const uuid = require('uuid/v4');
const expect = require('chai').expect;
const sinon = require('sinon');

describe('Repository Unit Test', function () {
    it('should create new sessions', function (done) {
        this.timeout(1000);
        const id = uuid();
        const repository = new Repository();
        repository.once('update', (sessions) => {
            expect(sessions).to.have.lengthOf(1);
            expect(sessions[0]).to.deep.include({ server_name: 'test', server_port: 58585, id });
            done();
        });
        repository.create({ server_name: 'test', server_port: 58585 }, id);
        clearInterval(repository.timeout);
    });

    it('should create more than one new session and not mix them up', function () {
        const idA = uuid();
        const idB = uuid();
        const idC = uuid();
        const idD = uuid();

        const repository = new Repository();
        repository.create({ server_name: 'test', server_port: 58585 }, idA);
        repository.create({ server_name: 'test 2', server_port: 58851 }, idB);
        repository.create({ server_name: 'test 3', server_port: 58852 }, idC);
        repository.create({ server_name: 'test 4', server_port: 58853 }, idD);

        const sessions = repository.getSessions();
        // verify all sessions have been correctly added
        expect(sessions).to.have.lengthOf(4);
        expect(sessions[0]).to.deep.include({ server_name: 'test', server_port: 58585 }, idA);
        expect(sessions[1]).to.deep.include({ server_name: 'test 2', server_port: 58851 }, idB);
        expect(sessions[2]).to.deep.include({ server_name: 'test 3', server_port: 58852 }, idC);
        expect(sessions[3]).to.deep.include({ server_name: 'test 4', server_port: 58853 }, idD);

        clearInterval(repository.timeout);
    });

    it('should apply incremental updates', function () {
        const id = uuid();
        const repository = new Repository();
        repository.create({ server_name: 'test', server_port: 11111 }, id);

        // run first update on the session and test afterwards
        const sessions = repository.getSessions();
        repository.update({ server_port: 22222 }, id);
        expect(sessions).to.have.lengthOf(1);
        expect(sessions[0]).to.deep.include({ server_name: 'test', server_port: 22222, id });

        // run second update and verify again
        repository.update({ server_port: 33333 }, id);
        expect(sessions).to.have.lengthOf(1);
        expect(sessions[0]).to.deep.include({ server_name: 'test', server_port: 33333, id });

        // end
        clearInterval(repository.timeout);
    });

    it('should not apply protected fields', function () {
        const id = uuid();
        const repository = new Repository();
        repository.create({ server_name: 'test' }, id);
        repository.update({ id: 'bad-id' }, id);

        const sessions = repository.getSessions();
        expect(sessions).to.have.lengthOf(1);
        expect(sessions[0]).to.deep.include({ server_name: 'test', id });
        clearInterval(repository.timeout);
    });

    it('should time out sessions after two minutes', function () {
        this.timeout(1000);
        const clock = sinon.useFakeTimers({ now: 1000 });
        const repository = new Repository();
        after(() => {
            // unset interval to avoid hanging tests
            clearInterval(repository.timeout);
            clock.uninstall();
        });
        expect(repository.getSessions()).to.be.empty;
        repository.create({ server_name: 'should-not-expire', server_port: 58585, run: true }, 'should-not-expire');
        repository.create({ server_name: 'should-not-expire-idle', server_port: 58585 }, 'should-not-expire-idle');
        repository.create({ server_name: 'should-expire', server_port: 58585, run: true }, 'should-expire');
        // verify sessions have been created
        let sessions = repository.getSessions();
        expect(sessions).to.have.lengthOf(3);
        expect(sessions[0]).to.deep.include({ server_name: 'should-not-expire', id: 'should-not-expire' });
        expect(sessions[1]).to.deep.include({ server_name: 'should-not-expire-idle', id: 'should-not-expire-idle' });
        expect(sessions[2]).to.deep.include({ server_name: 'should-expire', id: 'should-expire' });
        // step ahead and simulate update to should-not-expire
        clock.tick(100000);
        repository.update({}, 'should-not-expire');
        // verify sessions are still open
        sessions = repository.getSessions();
        expect(sessions).to.have.lengthOf(3);
        expect(sessions[0]).to.deep.include({ server_name: 'should-not-expire', id: 'should-not-expire' });
        expect(sessions[1]).to.deep.include({ server_name: 'should-not-expire-idle', id: 'should-not-expire-idle' });
        expect(sessions[2]).to.deep.include({ server_name: 'should-expire', id: 'should-expire' });
        // step ahead, there now should only be one session
        clock.tick(100000);
        sessions = repository.getSessions();
        expect(sessions).to.have.lengthOf(2);
        expect(sessions[0]).to.deep.include({ server_name: 'should-not-expire', id: 'should-not-expire' });
        expect(sessions[1]).to.deep.include({ server_name: 'should-not-expire-idle', id: 'should-not-expire-idle' });
        clock.uninstall();
    });

    it('should emit a deleted event on session end', function (done) {
        this.timeout(1000);
        const id = uuid();
        const repository = new Repository();
        repository.once('ended', (session) => {
            expect(session).to.include({ server_name: 'test', server_port: 58585, server_addr: '1.1.1.1', id });
            done();
        });
        repository.create({ server_name: 'test', server_port: 58585, server_addr: '1.1.1.1' }, id);
        repository.alive('1.1.1.1', false);
        clearInterval(repository.timeout);
    });
});
