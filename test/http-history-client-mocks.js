// https://github.com/BedrockStreaming/superagent-mock#usage
module.exports = [
    {
        pattern: 'https://history-service.emergencyx.de(.*)',

        fixtures: function (match, params, headers, context) {
            if (match[1] === '/500') {
                throw new Error(500);
            }
            if (headers['Authorization'] !== 'valid-token') {
                throw new Error(401);
            }
            
            return params;
        },

        post: function (match, data) {
            return {
                data: data,
                status: 201
            };
        }
    },
];
